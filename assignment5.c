#include <stdio.h>

#define MainPrice 15
#define Mainattendees 120
#define PerformanceCost 500
#define AttendeeCost 3

double calculateRevenue(double price, int attendees){
    return (PerformanceCost+(AttendeeCost*attendees));
}

double calculateTotal(double price, int attendees){
    return (price*attendees);
}

double calculateProfit(double revenue, double total){
    return (total-revenue);
}

int calculateAttendees(double price){
    return 120-(price-15)/5*20;
}

int main(){
    for(double price=5;price<=50;price+=5){
        printf("Price of a ticket:  %.2f\n",price);
        double revenue=calculateRevenue(price,calculateAttendees(price));
        double total=calculateTotal(price,calculateAttendees(price));
        printf("Number of Attendees: %d\n",calculateAttendees(price));
        printf("Profit: %.2f\n\n",calculateProfit(revenue,total));
    }

}
